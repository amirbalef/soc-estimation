import numpy as np
from torch.utils.data import Dataset
from torchvision import transforms

import torch
import pandas as pd
import numpy as np
from mat4py import loadmat
from torch.utils.data import Dataset, DataLoader
from sklearn import preprocessing

class PanasonicDataset(Dataset):
    def __init__(self, root,chamber_temp, sequence_length=1, normallizition = 0, avg = 0):
        self.data = loadmat(root)
        self.sequence_length = sequence_length

        # Panasonic Ah capacity
        self.BATTERY_AH_CAPACITY = 2.9000

        # Construct dataframe from MATLAB data
        self.df = pd.DataFrame(self.data)
        self.df = self.df.T
        self.df = self.df.apply(lambda x : pd.Series(x[0]))
        self.df = self.df.applymap(lambda x : x[0])

        #self.df = self.df[self.df["Chamber_Temp_degC"]==chamber_temp]
        # Clean up unnecessary columns
        del self.df['Chamber_Temp_degC']
        del self.df['TimeStamp']
        del self.df['Time']
        del self.df['Power']
        del self.df['Wh']
        
        if(avg):
            self.df['rol_Voltage'] = self.df['Voltage'].rolling(sequence_length, min_periods=1).mean()
            self.df['rol_Current'] = self.df['Current'].rolling(sequence_length, min_periods=1).mean()

        # Add SOC column
        ah = self.df['Ah']
        self.df['SOC'] = 1 + (ah/self.BATTERY_AH_CAPACITY)
        del self.df['Ah']

        
        # Convert data to numpy
        self.data = self.df.to_numpy(dtype=np.float32)

        # Set values for dataset
        self.x = torch.from_numpy(self.data[:,:-1])
        if (normallizition):
            self.x -= self.x.min(1, keepdim=True)[0]
            self.x = 2 * self.x/self.x.max(1, keepdim=True)[0] - 1
        self.y = torch.from_numpy(self.data[:,-1])

        # Reshape to match required label tensor shape
        self.y = self.y.reshape((len(self.y), 1))

    def __getitem__(self, idx):
        # Return window with the state of charge of last element in window (time series data + state of charge at the very end)
        start = idx 
        end = idx + self.sequence_length
        return self.x[start:end, :], self.y[end - 1]
    def __len__(self):
        # Subtract sequence length to ensure we don't sample out of bounds
        return len(self.data) - self.sequence_length + 1
