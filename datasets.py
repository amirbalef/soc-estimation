import numpy as np
from torch.utils.data import Dataset
from torchvision import transforms

import torch
import pandas as pd
import numpy as np
from mat4py import loadmat
from torch.utils.data import Dataset, DataLoader
from sklearn import preprocessing
from Datasets.panasonicDataset import PanasonicDataset

def Panasonic(sequence_length,chamber_temp,train_files,valid_files , avg = 0):
    # Dataset
    if(chamber_temp==25):
        dataset_dir = "Datasets/Panasonic 18650PF Data/Panasonic 18650PF Data/25degC/Drive cycles/"
        files={}
        files["Cycle_1"] = ["03-18-17_02.17 25degC_Cycle_1_Pan18650PF.mat"]
        files["Cycle_2"] = ["03-19-17_03.25 25degC_Cycle_2_Pan18650PF.mat"]
        files["Cycle_3"] = ["03-19-17_09.07 25degC_Cycle_3_Pan18650PF.mat"]
        files["Cycle_4"] = ["03-19-17_14.31 25degC_Cycle_4_Pan18650PF.mat"]
        files["NN"] = ["03-21-17_16.27 25degC_NN_Pan18650PF.mat"]
        files["HWFT"] = ["03-20-17_05.56 25degC_HWFTa_Pan18650PF.mat",
                       "03-20-17_19.27 25degC_HWFTb_Pan18650PF.mat"]
        files["US06"] = ["03-20-17_01.43 25degC_US06_Pan18650PF.mat"]
        files["UDDS"] = ["03-21-17_00.29 25degC_UDDS_Pan18650PF.mat"]
        files["LA92"] = ["03-21-17_09.38 25degC_LA92_Pan18650PF.mat"]
        
    if(chamber_temp==0):
        dataset_dir = "Datasets/Panasonic 18650PF Data/0degC/Drive cycles/"
        files={}
        files["Cycle_1"] = ["05-30-17_12.56 0degC_Cycle_1_Pan18650PF.mat"]
        files["Cycle_2"] = ["05-30-17_20.16 0degC_Cycle_2_Pan18650PF.mat"]
        files["Cycle_3"] = ["06-01-17_15.36 0degC_Cycle_3_Pan18650PF.mat"]
        files["Cycle_4"] = ["06-01-17_22.03 0degC_Cycle_4_Pan18650PF.mat"]
        files["NN"] = ["06-01-17_10.36 0degC_NN_Pan18650PF.mat"]
        files["HWFT"] = ["06-02-17_10.43 0degC_HWFET_Pan18650PF.mat"]
        files["US06"] = ["06-02-17_04.58 0degC_US06_Pan18650PF.mat"]
        files["UDDS"] = ["06-02-17_17.14 0degC_UDDS_Pan18650PF.mat"]
        files["LA92"] = ["06-01-17_10.36 0degC_LA92_Pan18650PF.mat"]
      
    if(chamber_temp==10):
        dataset_dir = "Datasets/Panasonic 18650PF Data/10degC/Drive cycles/"
        files={}
        files["Cycle_1"] = ["03-28-17_12.51 10degC_Cycle_1_Pan18650PF.mat"]
        files["Cycle_2"] = ["03-28-17_18.18 10degC_Cycle_2_Pan18650PF.mat"]
        files["Cycle_3"] = ["04-05-17_17.04 10degC_Cycle_3_Pan18650PF.mat"]
        files["Cycle_4"] = ["04-05-17_22.50 10degC_Cycle_4_Pan18650PF.mat"]
        files["NN"] = ["03-27-17_09.06 10degC_NN_Pan18650PF.mat"]
        files["HWFT"] = ["03-27-17_09.06 10degC_HWFET_Pan18650PF.mat"]
        files["US06"] = ["03-27-17_09.06 10degC_US06_Pan18650PF.mat"]
        files["UDDS"] = ["03-27-17_09.06 10degC_UDDS_Pan18650PF.mat"]
        files["LA92"] = ["03-27-17_09.06 10degC_LA92_Pan18650PF.mat"]
        
    train_sets=[]
    for key in train_files:
        train_datasets = []
        for f in files[key]:
            temp = PanasonicDataset(dataset_dir + f, chamber_temp, sequence_length, avg = avg)
            train_datasets.append(temp)
        train_set = torch.utils.data.ConcatDataset(train_datasets)
        train_sets.append(train_set)
        
    valid_sets=[]
    for key in valid_files:
        valid_datasets = []
        for f in files[key]:
            temp = PanasonicDataset(dataset_dir + f, chamber_temp, sequence_length, avg = avg)
            valid_datasets.append(temp)
        valid_set = torch.utils.data.ConcatDataset(valid_datasets)
        valid_sets.append(valid_set)

    return train_sets,valid_sets