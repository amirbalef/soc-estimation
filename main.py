from comet_ml import Experiment

import os
import argparse

import numpy as np
np.random.seed(0)

import torch
torch.manual_seed(0)

from torch.utils.data import DataLoader
import torch.optim as optim
import torch.nn as nn

from Models import common, gpr
import datasets
from train import train, lr_decay
from trainGPR import trainGPR


def main(hyper_params):
    
    epochs = hyper_params['epochs']
    batch_size = hyper_params['batch_size']
    learning_rate = hyper_params['learning_rate']
    sequence_length = hyper_params['sequence_length']
    chamber_temp = hyper_params['chamber_temp']
    num_classes = 1
    
    avg = 0
    if hyper_params['model']== 'mlp':
        avg = 1
    
    input_feature = 3 + avg * 2 
    if(len(hyper_params['api_key'])>0):
        # Create an experiment with your api key:
        experiment = Experiment(
            api_key=hyper_params['api_key'],
            project_name="soc-estimation",
            workspace="amirbalef",
        )
    else:
        experiment =  Experiment(api_key="dummy", disabled=True)
    experiment.log_parameters(hyper_params)
    
    train_files = ["Cycle_1","Cycle_2","Cycle_3","Cycle_4","NN"]
    valid_files = ["HWFT","US06","UDDS","LA92"]

    train_sets ,valid_sets = datasets.Panasonic(sequence_length,chamber_temp,train_files,valid_files , avg = avg )
    
    train_set = torch.utils.data.ConcatDataset(train_sets)
    train_loader = DataLoader(dataset=train_set, batch_size=batch_size, shuffle=True)
    
    valid_loaders = []
    for d in valid_sets:
        temp = DataLoader(dataset=d, batch_size=batch_size, shuffle=True)
        valid_loaders.append(temp)
    valid_loader = (valid_files,valid_loaders)
    
    
    if torch.cuda.is_available():
      device=torch.device('cuda:0')
    else:
      device=torch.device('cpu')

    if hyper_params['model']== 'mlp':
        input_size = input_feature*sequence_length
        hidden_size = 10*sequence_length
        noise_std = 0.005
        model = common.NeuralNet(input_size, hidden_size, num_classes, noise_std)
        loss_fn = nn.MSELoss()
        model = model.to(device)
        optimizer = optim.Adam(model.parameters(), lr=learning_rate)
        train(device, model, loss_fn, train_loader, valid_loader, epochs, optimizer, lr_decay, experiment) 

    if hyper_params['model']== 'gru':
        input_size = input_feature
        num_layers=3
        hidden_size = sequence_length
        noise_std = 0.005
        model = common.GRU(input_size, hidden_size,num_layers, num_classes, noise_std)
        loss_fn = nn.MSELoss()
        model = model.to(device)
        optimizer = optim.Adam(model.parameters(), lr=learning_rate)
        train(device, model, loss_fn, train_loader, valid_loader, epochs, optimizer, lr_decay, experiment)    
            
    if hyper_params['model']== 'gpr':
        input_size = 3*sequence_length
        inducing_points = torch.randn(1000,input_size)
        model = gpr.GPModel(inducing_points=inducing_points)
        likelihood = gpr.likelihood()

        if torch.cuda.is_available():
            model = model.cuda()
            likelihood = likelihood.cuda()

        
        optimizer = torch.optim.Adam([
            {'params': model.parameters()},
            {'params': likelihood.parameters()},
        ], lr=learning_rate)
        
        loss_fn = gpr.loss_fn(likelihood,model,len(train_loader))
        
        trainGPR(device, model, likelihood, loss_fn,train_loader, valid_loader, epochs, optimizer, batch_size, input_size, experiment)    
        
    if hyper_params['model']== 'dgpr':
        input_size = 3*sequence_length
        inducing_points = torch.randn(1000,input_size)
        
        feature_extractor = gpr.LargeFeatureExtractor(input_size)
        model = gpr.DeepGPModel(inducing_points=inducing_points,feature_extractor=feature_extractor)
        likelihood = gpr.likelihood()

        if torch.cuda.is_available():
            model = model.cuda()
            likelihood = likelihood.cuda()

        optimizer = torch.optim.Adam([
            {'params': model.parameters()},
            {'params': likelihood.parameters()},
        ], lr=learning_rate)
        
        loss_fn = gpr.deep_loss_fn(likelihood,model,len(train_loader))
        
        trainGPR(device, model, likelihood, loss_fn,train_loader, valid_loader, epochs, optimizer, batch_size, input_size, experiment)    
        
    if hyper_params['model']== 'gru-gpr':
        input_size = input_feature*sequence_length
        inducing_points = torch.randn(1000,input_size)
        
        feature_extractor = gpr.GRUNet(sequence_length, input_feature, 1000, 50, 1)
        model = gpr.DeepGPModel(inducing_points=inducing_points,feature_extractor=feature_extractor)
        likelihood = gpr.likelihood()

        if torch.cuda.is_available():
            model = model.cuda()
            likelihood = likelihood.cuda()

        optimizer = torch.optim.Adam([
            {'params': model.parameters()},
            {'params': likelihood.parameters()},
        ], lr=learning_rate)
        
        loss_fn = gpr.deep_loss_fn(likelihood,model,len(train_loader))
        
        trainGPR(device, model, likelihood, loss_fn,train_loader, valid_loader, epochs, optimizer, batch_size, input_size, experiment)    
        

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--api_key", type=str, default="")
    parser.add_argument("--dataset", type=str, default="Panasonic", choices=["Panasonic"])
    parser.add_argument("--model", type=str, default="mlp", choices=["mlp","gru","gpr","dgpr","gru-gpr"])
    parser.add_argument("--batch_size", type=int, default=32)
    parser.add_argument("--learning_rate", type=float, default=2e-4, help="learning rate")
    parser.add_argument("--epochs", type=int, default=20)
    parser.add_argument("--optimizer", type=str, default="ADAM",choices=["ADAM"])
    parser.add_argument("--sequence_length", type=int, default=5)
    parser.add_argument("--chamber_temp", type=int, default=25)
    args = parser.parse_args()

    print("=" * 80)
    print("Summary of training process:")
    print("Batch size: {}".format(args.batch_size))
    print("Learing rate       : {}".format(args.learning_rate))
    print("Number of epochs       : {}".format(args.epochs))
    print("Dataset       : {}".format(args.dataset))
    print("Local Model       : {}".format(args.model))
    print("=" * 80)

    hyper_params = {
        "api_key": args.api_key,
        "dataset": args.dataset,
        "model": args.model,
        "batch_size": args.batch_size,
        "learning_rate": args.learning_rate,
        "epochs": args.epochs,
        "optimizer": args.optimizer,
        "chamber_temp": args.chamber_temp,
        "sequence_length": args.sequence_length
    }
    main(hyper_params)
