import numpy as np
import torch
from tqdm import tqdm_notebook as tqdm

def train(device, model, loss_fn, train_loader, valid_loader, epochs, optimizer, change_lr=None,experiment=None):
  for epoch in tqdm(range(1,epochs+1)):
      with experiment.train():
        model.train()
        batch_losses=[]
        trace_y = []
        trace_yhat = []
        if change_lr:
          optimizer = change_lr(optimizer, epoch)
        for i, data in enumerate(train_loader):
          x, y = data
          optimizer.zero_grad()
          x = x.to(device)
          y = y.to(device)
          y_hat = model(x)
          trace_y.append(y.cpu().detach().numpy())
          trace_yhat.append(y_hat.cpu().detach().numpy())   
          loss = loss_fn(y_hat, y)
          loss.backward()
          batch_losses.append(loss.item())
          optimizer.step()
        trace_y = np.concatenate(trace_y)
        trace_yhat = np.concatenate(trace_yhat)
        MAE = np.mean(np.abs(trace_yhat-trace_y))
        MAX = np.max(np.abs(trace_yhat-trace_y))
        experiment.log_metric("loss(MSE)",np.mean(batch_losses), epoch=epoch)
        experiment.log_metric("MAE",MAE, epoch=epoch)
        print(f'Epoch:{epoch} Train-Loss(MSE) : {np.mean(batch_losses)} MAE:{MAE*100:.2f}%  MAX:{MAX*100:.2f}%')
        
      if(epoch%10 == 0):    
        with experiment.validate():
            (valid_files,valid_loaders) = valid_loader
            result=[]
            for method, valid_data in zip(valid_files, valid_loaders):
                model.eval()
                batch_losses=[]
                trace_y = []
                trace_yhat = []
                for data in valid_data:
                  x, y = data
                  x = x.to(device)
                  y = y.to(device)
                  y_hat = model(x)
                  loss = loss_fn(y_hat, y)
                  trace_y.append(y.cpu().detach().numpy())
                  trace_yhat.append(y_hat.cpu().detach().numpy())      
                  batch_losses.append(loss.item())
                trace_y = np.concatenate(trace_y)
                trace_yhat = np.concatenate(trace_yhat)
                
                loss_MSE = np.mean(batch_losses)
                MAE = np.mean(np.abs(trace_yhat-trace_y))
                MAX = np.max(np.abs(trace_yhat-trace_y))
                
               
                result.append([loss_MSE,MAE,MAX])
                
                experiment.log_metric("loss(MSE)",loss_MSE, epoch=epoch)
                experiment.log_metric("MAE",MAE, epoch=epoch)
                print(f'Epoch:{epoch} Valid-Loss(MSE) : {loss_MSE}  MAE:{MAE*100:.2f}%  MAX:{MAX*100:.2f}%  method:{method}')
            
            print(f'Epoch:{epoch} Valid-Loss(MSE) : {np.mean(result,axis=0)[0]}  MAE:{np.mean(result,axis=0)[1]*100:.2f}%  MAX:{np.max(result,axis=0)[2]*100:.2f}% Total')



def setlr(optimizer, lr):
  for param_group in optimizer.param_groups:
    param_group['lr'] = lr
  return optimizer

def lr_decay(optimizer, epoch):
  for param_group in optimizer.param_groups:
    learning_rate = param_group['lr']
  if epoch%40==0:
    new_lr = learning_rate / (10**(epoch//10))
    optimizer = setlr(optimizer, new_lr)
    print(f'Changed learning rate to {new_lr}')
  return optimizer
