import numpy as np
import torch
import gpytorch
from tqdm import  tqdm

def trainGPR(device, model, likelihood, loss_fn, train_loader, valid_loader, epochs, optimizer,batch_size, input_size, experiment=None):
  for epoch in tqdm(range(1,epochs+1)):
    with experiment.train():
        model.train()
        likelihood.train()

        batch_losses=0
        MAE = 0
        MAX = 0
        count = 0
        minibatch_iter = tqdm(train_loader, desc="Minibatch", position=0, leave=True)
        for data in (minibatch_iter):
            x, y = data
            x = x.reshape(-1,input_size)
            y = y.reshape(-1)
            x = x.to(device)
            y = y.to(device)
            optimizer.zero_grad()
            y_hat = model(x)
            y_preds = likelihood(y_hat).mean
            dif = np.abs(y_preds.cpu().detach().numpy()-y.cpu().detach().numpy()) 
            MAE += np.mean(dif)
            MAX += np.max(dif)
            loss = -loss_fn(y_hat, y)
            batch_losses +=loss.item()
            minibatch_iter.set_postfix(loss=loss.item())
            loss.backward()
            optimizer.step()
            count = count + 1

        MAE /= count
        MAX /= count
        batch_losses /= count
        experiment.log_metric("loss(MSE)",batch_losses, epoch=epoch)
        experiment.log_metric("MAE",MAE, epoch=epoch)
        print(f'Epoch:{epoch} Train-Loss(MSE) : {batch_losses} MAE:{MAE*100:.2f}%  MAX:{MAX*100:.2f}%')
        
    if(epoch%10 == 0):    
      with experiment.validate(),torch.no_grad(), gpytorch.settings.num_likelihood_samples(batch_size):
        (valid_files,valid_loaders) = valid_loader
        result=[]
        for method, valid_data in zip(valid_files, valid_loaders):
            model.eval()
            likelihood.eval()
            batch_losses=0
            MAE = 0
            MAX = 0
            count = 0
            minibatch_iter = tqdm(valid_data, desc="Validation Minibatch", position=0, leave=False)
            for data in minibatch_iter:
              x, y = data
              x = x.reshape(-1,input_size)
              y = y.reshape(-1)
              x = x.to(device)
              y = y.to(device)
              y_hat = model(x)
              loss = -loss_fn(y_hat, y)
              y_preds = likelihood(y_hat).mean
              dif = np.abs(y_preds.cpu().detach().numpy()-y.cpu().detach().numpy()) 
              MAE += np.mean(dif)
              MAX += np.max(dif)   
              batch_losses +=loss.item()
              count = count + 1

            MAE /= count
            MAX /= count
            batch_losses /= count
            
            result.append([batch_losses,MAE,MAX])
            
            experiment.log_metric("loss(MSE)",batch_losses, epoch=epoch)
            experiment.log_metric("MAE",MAE, epoch=epoch)
            print(f'Epoch:{epoch} Valid-Loss(MSE) : {batch_losses}  MAE:{MAE*100:.2f}%  MAX:{MAX*100:.2f}%  method:{method}')

        print(f'Epoch:{epoch} Valid-Loss(MSE) : {np.mean(result,axis=0)[0]}  MAE:{np.mean(result,axis=0)[1]*100:.2f}%  MAX:{np.max(result,axis=0)[2]*100:.2f}% Total')

